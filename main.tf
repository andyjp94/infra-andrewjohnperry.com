data "aws_acm_certificate" "top_level" {
  domain      = "${var.certificate_name}"
  types       = ["AMAZON_ISSUED"]
  most_recent = true
  provider    = "aws.virginia"
}

data "aws_route53_zone" "self" {
  name = "${var.certificate_name}."
}

resource "aws_s3_bucket" "logs" {
  bucket = "ajp-logs"
}

module "production" {
  source         = "github.com/andyjp94/website-infra.git?ref=development"
  domain_name    = "${var.domain_name}"
  hosted_zone_id = "${data.aws_route53_zone.self.zone_id}"
  certificate    = "${data.aws_acm_certificate.top_level.arn}"
  log_bucket     = "${aws_s3_bucket.logs.bucket_domain_name}"

  lambda = {
    name      = "production-security-headers"
    file_path = "${path.module}/headers.zip"
    handler   = "headers.handler"
    runtime   = "nodejs8.10"
  }
}

module "staging" {
  source         = "github.com/andyjp94/website-infra.git?ref=development"
  domain_name    = "${var.staging_domain_name}"
  hosted_zone_id = "${data.aws_route53_zone.self.zone_id}"
  certificate    = "${data.aws_acm_certificate.top_level.arn}"
  log_bucket     = "${aws_s3_bucket.logs.bucket_domain_name}"
  www            = false

  lambda = {
    name      = "staging-security-headers"
    file_path = "${path.module}/headers.zip"
    handler   = "headers.handler"
    runtime   = "nodejs8.10"
  }
}
